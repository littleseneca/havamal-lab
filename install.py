### Imports ###
import os
import subprocess
subprocess.run("pip3 install pyyaml paramiko pandas Jinja2", shell=True, capture_output=True)
import paramiko
import yaml
import pandas
import numpy as np
import shutil
import time
from collections import Counter

###### Load Seed Variables from seed-variables.yml
with open('seed-variables.yml') as info:
    yaml_seed_variables = yaml.safe_load(info)
###### /Load Seed Variables from seed-variables.yml

### /Imports ###

### Functions ###
### Comment Block Formatter
def comment_block(x):
    print("##########")
    print(x)
    print("##########")
    print()
### /Comment Block Formatter

### Command Runner
def command_runner(x):
    subprocess.run(x, shell=True, capture_output=True)
### /Command Runner

### Validate Ansible Installation
def ansible_install():
    comment_block("Checking Ansible Installation Status")
    ansible_path = "/usr/bin/ansible"
    ansible_path_test = os.path.exists(ansible_path)
    # Check to see if Ansible is already installed
    if ansible_path_test == True:
        comment_block("Ansible is already Installed")
    # If not already installed, deploy Ansible
    else:
        comment_block("Installing Ansible")
        command_runner("apt install ansible -y")
        comment_block("Ansible Installed")
### /Validate Ansible Installation

### Validate Terraform Installation
##### Check to see if Terraform is already installed
def terraform_install():
    terraform_repo = "/etc/apt/sources.list.d/hashicorp.list"
    terraform_repo_test = os.path.exists(terraform_repo)
    comment_block("Checking Terraform Installation Status")
    # If Terraform is not already installed, begin install
    if terraform_repo_test == True:
        comment_block("Terraform is already installed")
        return
    else:
        # Configure Pre-reqs for install
        comment_block("Installing Terraform")
        command_runner("apt-get update && sudo apt-get install -y gnupg software-properties-common")
        # Configure Terraform Repository
        command_runner("wget -O- https://apt.releases.hashicorp.com/gpg | gpg --dearmor | tee /usr/share/keyrings/hashicorp-archive-keyring.gpg")
        command_runner("echo \"deb [signed-by=/usr/share/keyrings/hashicorp-archive-keyring.gpg] https://apt.releases.hashicorp.com $(lsb_release -cs) main\" | tee /etc/apt/sources.list.d/hashicorp.list")
        # Install Terraform
        command_runner("apt update && apt install terraform")
        comment_block("Terraform Installed")
### /Validate Terraform Installation

### Check if tailscale is installed and deploy if not
def tailscale_install():
    tailscale_path = "/usr/bin/tailscale"
    tailscale_path_reported = command_runner("which tailscale")
    comment_block("Checking Tailscale Installation Status")
    if tailscale_path_reported != tailscale_path:
        # Stand Up Tailscale for the first time
        comment_block("Installing Tailscale and Configuring")
        command_runner("curl -fsSL https://tailscale.com/install.sh | sh")
        command_runner("echo 'net.ipv4.ip_forward = 1' | sudo tee -a /etc/sysctl.conf")
        command_runner("echo 'net.ipv6.conf.all.forwarding = 1' | sudo tee -a /etc/sysctl.conf")
        command_runner("sudo sysctl -p /etc/sysctl.conf")
        command_runner("sudo tailscale up --authkey {}".format(yaml_seed_variables['hypervisor_parity']))
        command_runner("tailscale up --advertise-exit-node")
        comment_block("Tailscale Installed")
    elif tailscale_path_reported == tailscale_path:
        # Reprovision existing tailscale instance
        comment_block("Tailscale Installed, Configuring")
        command_runner("echo 'net.ipv4.ip_forward = 1' | sudo tee -a /etc/sysctl.conf")
        command_runner("echo 'net.ipv6.conf.all.forwarding = 1' | sudo tee -a /etc/sysctl.conf")
        command_runner("sudo sysctl -p /etc/sysctl.conf")
        command_runner("sudo tailscale up --authkey {}".format(yaml_seed_variables['hypervisor_parity']))
        command_runner("tailscale up --advertise-exit-node")
        comment_block("Tailscale Installed")
### /Check if tailscale is installed and deploy if not

### Validate Proxmox Installation
def proxmox_status():
    comment_block("Checking if Proxmox is Viable")
    # Check to make sure that pveversion is above 7.0 as 7.1 is the version used to test Havamal Lab
    test = subprocess.run("pveversion --verbose | head -1 | awk '{print $2}' | sed 's/-.*//'", shell=True, capture_output=True)
    if float(test.stdout) > 7.0:
        comment_block("Proxmox is Viable")
    else:
        # If version is older than 7.01, then quit the deployment
        print("This OS is NOT Supported. Must be running Proxmox Version 7.1 or higher")
        quit()
### /Validate Proxmox Installation

### Build Environment Knowledge
### Build Environment Knowledge - List Quarem
def proxmox_environment_quarem():
    # Build a list of devices currently provisioned within a quarem
    ip_addr_raw = subprocess.run("pvecm status | grep -A20 \"Membership information\" | tail -n +4 | awk \'{print $3}\'", shell=True, capture_output=True)
    ip_addr_str = str(ip_addr_raw.stdout)
    #Format Results for list contents
    ip_addr_str = ip_addr_str.replace('b', '')
    ip_addr_str = ip_addr_str.replace('\'','')
    ip_addr_array = ip_addr_str.split("\\n")
    ip_addr_array = ip_addr_array[:-1]
    if not ip_addr_array:
        return(ip_addr_array)
    else:
        return(ip_addr_array)

### /Build Environment Knowledge - List Quarem

### Build Environment Knowledge - Learn Quarem

###### List CPU Information on a Per Device Basis using LSCPU 
def proxmox_environment_quarem_cpu(x):
    # Take IP Address List Item and use that value to SSH into Device
    ip_list = x
    # Use the private key of this clustered node to authenticate connection
    pkey = paramiko.RSAKey.from_private_key_file("/root/.ssh/id_rsa")
    ssh = paramiko.SSHClient()
    # Auto-Add Device to approved paramiko hosts
    ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    cpu_list = []
    # Itterate through members of the ip_list to find CPU information
    for i in ip_list:
        ssh.connect(i, username='root', pkey=pkey)
        stdin, stdout, stderr = ssh.exec_command("lscpu | grep CPU | head -2 | tail -1 | awk '{print $2}' | tr -d '\n'")
        lines = str(stdout.readlines())
        lines = lines.replace('[', '') 
        lines = lines.replace(']', '') 
        lines = lines.replace('\'', '') 
        lines = int(lines)
        cpu_list.append(lines)
    return(cpu_list)
###### /List CPU Information on a Per Device Basis using LSCPU 

###### List RAM Information on a Per Device Basis using FREE
def proxmox_environment_quarem_ram(x):
    # Take IP Address List Item and use that value to SSH into Device
    ip_list = x
    # Grab private key from root local device to validate members of quarem
    pkey = paramiko.RSAKey.from_private_key_file("/root/.ssh/id_rsa")
    ssh = paramiko.SSHClient()
    # Auto-Add Device to approved paramiko hosts
    ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    ram_list = []
    # Itterate through members of the ip_list to find RAM information
    for i in ip_list:
        ssh.connect(i, username='root', pkey=pkey)
        stdin, stdout, stderr = ssh.exec_command("free -m | head -2 | tail -1 | awk '{print $2}' | tr -d '\n'")
        lines = str(stdout.readlines())
        #Format Results for list contents
        lines = lines.replace('[', '') 
        lines = lines.replace(']', '') 
        lines = lines.replace('\'', '')
        lines = int(lines)
        ram_list.append(lines)  
    return(ram_list)
###### /List RAM Information on a Per Device Basis using FREE

###### List HOSTNAME Information on a Per Device Basis using HOSTNAME
def proxmox_environment_quarem_host(x):
    # Take IP Address List Item and use that value to SSH into Device
    ip_list = x
    # Grab private key from root local device to validate members of quarem
    pkey = paramiko.RSAKey.from_private_key_file("/root/.ssh/id_rsa")
    ssh = paramiko.SSHClient()
    # Auto-Add Device to approved paramiko hosts
    ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    host_list = []
    # Itterate through members of the ip_list to find hostname information
    for i in ip_list:
        ssh.connect(i, username='root', pkey=pkey)
        stdin, stdout, stderr = ssh.exec_command("hostname | tr -d '\n'")
        lines = str(stdout.readline())
        host_list.append(lines)
    return(host_list)

###### /List HOSTNAME Information on a Per Device Basis using HOSTNAME

###### Aggrigate Proxmox Quarim Information into a unified Dictionary
def proxmox_environment_quarem_dic(x,y,z,q):
    data = {
        "hostnames": [x],
        "ip address": [y],
        "cpu": [z],
        "ram": [q]
    }
    df = pandas.DataFrame(list(zip(x,y,z,q)), columns=['Hostname', 'IP Addr', 'CPU#', 'RAM'])
    return(df)
###### /Aggrigate Proxmox Quarim Information into a unified Dictionary

###### Validate hypervisor_parity value from seeded variables
def parity_check(x,y): 
    # Check if the number of available nodes matches the parity or is larger than the parity
    if x <= len(y):
        comment_block("Valid Parity")
        deploy_model = yaml_seed_variables['hypervisor_parity'] 
        return(deploy_model)

    # Check if the number of available nodes is less than the parity
    elif x > len(y):
        comment_block("Invalid Parity. Parity must be smaller than or equal to number of devices in Quarm")
        comment_block("Exiting")
        quit()

    # Default failure Message
    else:
        comment_block("Invalid Parity. Parity must be an integer greater than or equal to 2")
        comment_block("Exiting")
        quit()
###### /Validate hypervisor_parity value from seeded variables

### /Build Environment Knowledge - Learn Quarem

### Filter Quarem Dictionary results

###### Filter Quarem Dictionary for nodes with CPU threads greater than or equal to 12
def quarem_cpu_calculator(x):
    limit = 12
    column_name = 'CPU#'
    column = x[column_name]
    count = column[column >= limit].count()
    return(count)
###### /Filter Quarem Dictionary for nodes with CPU threads greater than or equal to 12

###### Filter Quarem Dictionary for nodes with RAM greater than 32768mb
def quarem_ram_calculator(x):
    limit = 32768
    column_name = 'RAM'
    column = x[column_name]
    count = column[column > limit].count()
    return(count)
###### /Filter Quarem Dictionary for nodes with RAM greater than 32768mb

###### Cross compare selected nodes for both RAM and CPU success
def quarem_targets(x):
    cpu_limit = 4
    ram_limit = 32768
    results=[]
    for i in x.index:
        if x['RAM'][i] > ram_limit:
            results.append(x['Hostname'][i])
        if x['CPU#'][i] > cpu_limit:
            results.append(x['Hostname'][i])
    return(results)
###### /Cross compare selected nodes for both RAM and CPU success

###### Validate that cross compared and selected nodes meet original requirements
def quarem_targets_export(x):
    results=[]
    for key, value in x.items():
        if value == 2:
            results.append(key)
    return(results)
###### /Validate that cross compared and selected nodes meet original requirements

### Provide Feedback to User concerning System elegibility for Havamal-Lab ###

###### Validate CPU threads and quit if quarem is inelegiable for Havamal-Lab
def quarem_cpu_results(x,y):
    z = y-x
    if x >= y:
        print("On first pass it appears your devices have sufficient CPU Threads")
        return(True)
    else:
        print("While there are sufficient devices in your cluster to meet your desired deployment size")
        print("An insufficient number of the devices in your quarem meet the minimumm requirements to run")
        print("the Havamal deployment. Please consider reducing your deployment size.")
        print("")
        print("Either modify the hypervisor_parity value in the seed-variables file down by: ",z)
        print("Or increase the CPU cores available on your hypervisors to at least 12 threads per node")
        quit()
###### /Validate CPU threads and quit if quarem is inelegiable for Havamal-Lab
def quarem_ram_results(x,y):
    z = y-x
    if x >= y:
        print("On first pass it appears your devices have sufficient RAM")
        return(True)
    else:
        print("While there are sufficient devices in your cluster to meet your desired deployment size")
        print("An insufficient number of the devices in your quarem meet the minimumm requirements to run")
        print("the Havamal deployment. Please consider reducing your deployment size.")
        print("")
        print("Either modify the hypervisor_parity value in the seed-variables file down by: ",z)
        print("Or increase the RAM available on your hypervisors to at least 32768mb per node")
        quit()
###### /Provide Feedback to User concerning System elegibility for Havamal-Lab against specific devices

###### Provide Feedback to User concerning general system elegibility given full cross controlled review of selected systems
def quarem_targets_export_cross_eval(x):
    if len(x) < yaml_seed_variables['hypervisor_parity']:
        print("On Final Evaluation, it appears that the targeted devices do not have sufficient resources to meet the requirements of Havamal-Lab")
        print("Please make sure that the targeted nodes have at least 4 cores and 32768mb of ram")
        quit()
    else:
        print("On final evaluation, your targeted devices are performant enough for havamal-lab to deploy successfully in a parity formation")
        print("")
###### /Provide Feedback to User concerning general system elegibility given full cross controlled review of selected systems

### Begin Havamal-Lab Deployment on targeted nodes
###### Configure Prerequisits for Terraform Deployments 
def terraform_target_configuration():
    host_list_array = []
    # This loop creates a list that lists the targeted hostnames
    for x in quarem_targets_export(quarem_targets_array_count):    
        host_list_array.append(x)
        print(x)
        print()
    # This loop turns that list into a file for use in an ansible template
    if os.path.exists("target-playbooks/lab-management/hostnames.yml"):
        os.remove("target-playbooks/lab-management/hostnames.yml")

    if os.path.exists("target-playbooks/lab-management/seed-variables.yml"):
        os.remove("target-playbooks/lab-management/seed-variables.yml")
    
    shutil.copy("seed-variables.yml", "target-playbooks/lab-management/seed-variables.yml")

    f = open("target-playbooks/lab-management/hostnames.yml", "a")
    f.write("hostnames:")
    f.write("\n")

    for i in host_list_array:
        f = open("target-playbooks/lab-management/hostnames.yml", "a")
        f.write("   - ")
        f.write(i)
        f.write("\n")

    first_octet = subprocess.run("cat /etc/network/interfaces | grep address | awk '{print $2}' | awk -F. \'{print $1}\' | sed 's/$/./'", shell=True, capture_output=True)
    first_octet_decode = first_octet.stdout.decode('UTF-8').replace('\n','')
    second_octet = subprocess.run("cat /etc/network/interfaces | grep address | awk '{print $2}' | awk -F. \'{print $2}\'", shell=True, capture_output=True)
    second_octet_decode = second_octet.stdout.decode('UTF-8').replace('\n','')
    combined_octet = str(first_octet_decode+second_octet_decode)

    #Format Results for list contents
    f = open("target-playbooks/lab-management/hostnames.yml", "a")
    f.write("ip_addr: " + "\"" + combined_octet + "\"")

###### /Configure Prerequisits for Terraform Deployments 

###### Re-Baseline targeted nodes by removing existing vms
def proxmox_targets_baseline_rm_vmids(x):
    # Take IP Address List Item and use that value to SSH into Device
    ip_list = x
    # Grab private key from root local device to validate members of quarem
    pkey = paramiko.RSAKey.from_private_key_file("/root/.ssh/id_rsa")
    ssh = paramiko.SSHClient()
    # Auto-Add Device to approved paramiko hosts
    ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    # Itterate through members of the ip_list to kill exiting virtual machines
    for i in ip_list:
        ssh.connect(i, username='root', pkey=pkey)

        stdin, stdout, stderr = ssh.exec_command("qm list | awk '{print $1}' | tail -n +2")
        lines = stdout.readlines()
        formatted_lines = [x[:-1] for x in lines]
        print(formatted_lines)
        for i in formatted_lines:
            stdin, stdout, stderr = ssh.exec_command("nohup qm unlock " + str(i))
            while int(stdout.channel.recv_exit_status()) != 0: time.sleep(1)
            stdin, stdout, stderr = ssh.exec_command("nohup qm stop " + str(i))
            while int(stdout.channel.recv_exit_status()) != 0: time.sleep(1)
            stdin, stdout, stderr = ssh.exec_command("nohup qm destroy " + str(i))
            while int(stdout.channel.recv_exit_status()) != 0: time.sleep(1)
###### /Re-Baseline targeted nodes by removing existing vms

###### Re-Baseline targeted nodes by provisioning All available hard drivers as local-lvm
def proxmox_targets_baseline_deprovision_harddrives(x):
    # Take IP Address List Item and use that value to SSH into Device
    ip_list = x
    # Grab private key from root local device to validate members of quarem
    # Itterate through members of the ip_list, finding PVE drives
    # This heuristic checks to see if a disk is part of PVE. If it is, the disk is skipped. 
    with open('target-playbooks/inventory', 'w') as inventory:
        inventory.write('[targets]')
    for i in ip_list:
        f = open("target-playbooks/inventory", "a")
        f.write("\n")
        f.write(i)
        f.write(" ansible_ssh_common_args='-o StrictHostKeyChecking=no'")
    f.close()

    process = subprocess.Popen(['ansible-playbook', 'target-playbooks/harddrives.yml', '-i', 'target-playbooks/inventory'], stdout=subprocess.PIPE, universal_newlines=True)
    stdout = process.communicate()[0]
    stdout_formated = str('STDOUT:{}'.format(stdout))
    print(stdout_formated)
    os.remove("target-playbooks/inventory")
###### /Re-Baseline targeted nodes by provisioning All available hard drivers as local-lvm

###### Re-Baseline targeted nodes by Removing All Remaining Virtual Machines and replacing them with required templates
def proxmox_targets_baseline_provision_templates(x):
    # Take IP Address List Item and use that value to SSH into Device
    ip_list = x
    # Grab private key from root local device to validate members of quarem
    # Itterate through members of the ip_list, finding PVE drives
    # This heuristic destroyes all available VMs from the target hosts, and the provisions required templates. 
    with open('target-playbooks/inventory', 'w') as inventory:
        inventory.write('[targets]')
    for i in ip_list:
        f = open("target-playbooks/inventory", "a")
        f.write("\n")
        f.write(i)
        f.write(" ansible_ssh_common_args='-o StrictHostKeyChecking=no'")
    f.close()

    process = subprocess.Popen(['ansible-playbook', 'target-playbooks/vmtemplates.yml', '-i', 'target-playbooks/inventory'], stdout=subprocess.PIPE, universal_newlines=True)
    stdout = process.communicate()[0]
    stdout_formated = str('STDOUT:{}'.format(stdout))
    print(stdout_formated)
    os.remove("target-playbooks/inventory")
###### /Re-Baseline targeted nodes by Removing All Remaining Virtual Machines and replacing them with required templates

###### Build targets variable list for terraform
def proxmox_targets_baseline_provision_templates(x):
    # Take IP Address List Item and use that value to SSH into Device
    ip_list = x
    # Grab private key from root local device to validate members of quarem
    # Itterate through members of the ip_list, finding PVE drives
    # This heuristic destroyes all available VMs from the target hosts, and the provisions required templates. 
    with open('target-playbooks/inventory', 'w') as inventory:
        inventory.write('[targets]')
    for i in ip_list:
        f = open("target-playbooks/inventory", "a")
        f.write("\n")
        f.write(i)
        f.write(" ansible_ssh_common_args='-o StrictHostKeyChecking=no'")
    f.close()

    process = subprocess.Popen(['ansible-playbook', 'target-playbooks/vmtemplates.yml', '-i', 'target-playbooks/inventory'], stdout=subprocess.PIPE, universal_newlines=True)
    stdout = process.communicate()[0]
    stdout_formated = str('STDOUT:{}'.format(stdout))
    print(stdout_formated)
    os.remove("target-playbooks/inventory")
###### /Re-Baseline targeted nodes by Removing All Remaining Virtual Machines and replacing them with required templates
###### Set up Inventory for Provisioning Viritual Machines in Ansible
def ansible_virtual_machines_inventory(x):
    # Find the first octet IP Interface of the Local Host
    first_octet = subprocess.run("cat /etc/network/interfaces | grep address | awk '{print $2}' | awk -F. \'{print $1}\' | sed 's/$/./'", shell=True, capture_output=True)
    first_octet_decode = first_octet.stdout.decode('UTF-8').replace('\n','')
    # Find the second octet IP Interface of the Local Host
    second_octet = subprocess.run("cat /etc/network/interfaces | grep address | awk '{print $2}' | awk -F. \'{print $2}\'", shell=True, capture_output=True)
    second_octet_decode = second_octet.stdout.decode('UTF-8').replace('\n','')
    # Find combine the first and second octet IP Interface of the Local Host
    combined_octet = str(first_octet_decode+second_octet_decode)
    # make a counter for 2X the parity count as the output IP addresses of virtual machines
    y = x*2
    # make a counter for 4X the parity count as the output IP addresses of virtual machines
    z = x*4
    # Write to the target-playbooks/inventory file
    with open('target-playbooks/inventory', 'w') as inventory:
    # Build the domain-controllers list of assets
        inventory.write('[domain-controllers]')
        inventory.write("\n")
        for i in range(0,x):
            inventory.write(combined_octet)
            inventory.write('.10.1')
            inventory.write(str(i))
            inventory.write(" ansible_ssh_common_args='-o StrictHostKeyChecking=no'")
            inventory.write("\n")
    # Build the storage-controllers list of assets
        inventory.write('[storage-controllers]')
        inventory.write("\n")
        for i in range(0,x):
            inventory.write(combined_octet)
            inventory.write('.10.2')
            inventory.write(str(i))
            inventory.write(" ansible_ssh_common_args='-o StrictHostKeyChecking=no'")
            inventory.write("\n")
    # Build the kubernetes-controllers list of assets
        inventory.write('[kubernetes-controllers]')
        inventory.write("\n")
        for i in range(0,y):
            inventory.write(combined_octet)
            inventory.write('.10.3')
            inventory.write(str(i))
            inventory.write(" ansible_ssh_common_args='-o StrictHostKeyChecking=no'")
            inventory.write("\n")
    # Build the kubernetes-workers list of assets
        inventory.write('[kubernetes-workers]')
        inventory.write("\n")
        for i in range(0,z):
            inventory.write(combined_octet)
            inventory.write('.10.4')
            inventory.write(str(i))
            inventory.write(" ansible_ssh_common_args='-o StrictHostKeyChecking=no'")
            inventory.write("\n")
###### /Set up Inventory for Provisioning Viritual Machines in Ansible
###### Spin up Virtual Machines
def terraform_virtual_machines_provision():
    process = subprocess.Popen(['ansible-playbook', 'target-playbooks/terraform.yml'], stdout=subprocess.PIPE, universal_newlines=True)
    stdout = process.communicate()[0]
    stdout_formated = str('STDOUT:{}'.format(stdout))
    print(stdout_formated)
###### /Spin up Virtual Machines
###### 
### /Functions ###

### Deployment ###


###### Checking Prerequisits
comment_block("Setting Pre-requisits")
###### Checking Proxmox State
proxmox_status()
###### Checking Ansible State
ansible_install()
###### Checking Terraaform State
terraform_install()
#####> Checking Tailscale State
#tailscale_install()
comment_block("Validating Seeded Variables")
###### Checking if Proxmox Quarim Parity matches value in seeded variables
parity_check(yaml_seed_variables['hypervisor_parity'],proxmox_environment_quarem())
###### Collect Proxmox Ram Information from Quarem devices
prox_ram = (proxmox_environment_quarem_ram(proxmox_environment_quarem()))
###### Collect Proxmox CPU Information from Quarem devices
prox_cpu = (proxmox_environment_quarem_cpu(proxmox_environment_quarem()))
###### Collect Proxmox Host Information from Quarem devices
prox_host = (proxmox_environment_quarem_host(proxmox_environment_quarem()))
###### Generate a Pandas Table containing information collected from Quarem devices
###### Table contains Hostname, CPU, RAM, and IP Addresses
quarem_stats = proxmox_environment_quarem_dic(prox_host,proxmox_environment_quarem(),prox_cpu,prox_ram)

comment_block("Validating quarem devices")
###### Sort Dictionary, looking for endpoints which meet CPU threads requirements 
quarem_cpu_results(quarem_cpu_calculator(quarem_stats),yaml_seed_variables['hypervisor_parity'])
###### Sort Dictionary, looking for endpoints which meet RAM  requirements 
quarem_ram_results(quarem_ram_calculator(quarem_stats),yaml_seed_variables['hypervisor_parity'])
###### Turn Table Results into an Array with the names of devices which meet requirements. 
###### Names that show up twice are devices that meet both the ram and cpu requirements 
quarem_targets_array = quarem_targets(quarem_stats)
###### Turn Array into a count (values for each index should be 2 if they meet both ram and cpu requirements)
quarem_targets_array_count = dict(Counter(quarem_targets_array))

comment_block("Targeting Nodes for deployment")
print("")

###### Evaluate Targets to see if the nodes that meet both ram and cpu requirements match original context 
###### If true, than print success message. If failure, educate user and close
quarem_targets_export_cross_eval(quarem_targets_export(quarem_targets_array_count))

print("Targeting the following Servers for the deployment of Havamal-Lab:")
print("")

###### List targeted nodes
terraform_target_configuration()
###### /List targeted nodes

###### Find IP Addresses of Targeted Hosts within quarem_stats table
quarem_targets_export_ip=[]
for x in quarem_targets_export(quarem_targets_array_count):
    quarem_targets_export_ip.append(str(quarem_stats.loc[quarem_stats['Hostname'] == str(x), 'IP Addr'].item()))
###### /Find IP Addresses of Targeted Hosts within quarem_stats table

### /Checking Prerequisits

### Configuring Targets
comment_block("Removing Existing VMs from targeted hosts")
###### Remove Exiting Virtual Machines from Targets
proxmox_targets_baseline_rm_vmids(quarem_targets_export_ip)
###### /Remove Exiting Virtual Machines from Targets
###### Provision Non-PVE Drives for Cluster
proxmox_targets_baseline_deprovision_harddrives(quarem_targets_export_ip)
###### /Provision Non-PVE Drives for Cluster
comment_block("Finished Building Storage Arrays")
comment_block("Stand By while we build VM Templates. This may take some time depending on your network speed.")
###### Provision Non-PVE Drives for Cluster
proxmox_targets_baseline_provision_templates(quarem_targets_export_ip)
###### /Provision Non-PVE Drives for Cluster
comment_block("Finished Building VM Templates")
###### Build the new inventory for ansible tasks
ansible_virtual_machines_inventory(yaml_seed_variables['hypervisor_parity'])
###### /Build the new inventory for ansible tasks
comment_block("Pushing Configurations to Build Virtual Environment using Terraform. This will take a while.")
###### Build Virtual Machines using Terraform
terraform_virtual_machines_provision()
###### /Build Virtual Machines using Terraform