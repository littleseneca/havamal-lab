# havamal-lab



## Getting started

This project is intended to build a fully production ready homelab, dynamically generated, given just a few basic inputs from you. 

Why should you use havamal-lab? Because once spun up, you will have a fully independent source of truth. 

(for the studious in the room, you will know that the Havamal is a poetry book of wisdom purportedly written by Odin, the chief god of the Nordic pantheon.)

What do I mean by Source of truth? 

All of your files, all of your content, all of your products and services, hosted under your own roof. 
This is a self hosting project to make the process of building a self dependent digital ecosystem less painful. 

## Requirements

To use this project successfully, you will need to have a few prerequisits sorted out.

### Hypervisor Requirements

First, you will absolutely need to have a server running proxmox set up. Without this step, we wont have anything to build to. 

To set up your own proxmox server, please review this link: 
https://www.proxmox.com/en/

### Hardware Requirements
Second, this server will have some minimal hardware requirements. Your server will need to have at least 64gb of ram, 128gb of high performance storage, and 8 cpu threads (4 cores).

You will also need to have at least two non-boot hard drives (preferably NVME SSDs, or at least SATA SSDs) connected to each target device. These devices must not be pre-formated or members of any arrays.

### Software Requirements
Outside of Proxmox, you will also need a tailscale account so that you can access your environment from outside of the havamal-lab network. 

To set up tailscale, please review this link:
https://tailscale.com

Once your account is set up, please set up a tailscale API Token using the link here:
https://login.tailscale.com/admin/settings/keys

Make sure that you set this API token to Reusable and NOT Ephemeral. 

## Installation
### Retrieving this project from Gitlab
Once you have met all of the requirements (proxmox and tailscale), download the github repo. To do this, log into your proxmox hypervisor as the default root account. Then, Clone this repository to the home folder of your root proxmox user account.

"cd /root/"
"git clone git@gitlab.com:littleseneca/havamal-lab.git"

### Setting Project variables
To deploy this project, please review the seed-variables.yml file located in the root of the havamal-lab git repo now located in /root/havamal-lab/seed-variables.yml. 

Review the variables listed within seed-variables.yml file, and then change them as needed.

### Kicking off
After you have set environmental variables, you may kick off the installation simply by running the install.py script located in the root directory of this folder.

To run this script, submit the following command:

python3 install.py

This script simply validates that terraform and ansible are both installed. If not, the script will install both products. Afterwords, the script kicks off terraform and ansible provisioning and configuring scripts.