terraform {
  required_providers {
    proxmox = {
      source = "telmate/proxmox"
    }
  }
}
provider "proxmox" {
  pm_api_url = var.pm_hypervisor_url
  pm_user = var.pm_user
  pm_password = var.pm_pass
  pm_tls_insecure = true
}
resource "proxmox_vm_qemu" "domain_controllers" {
  vmid              = 100+count.index
  count             = yamldecode(file("seed-variables.yml"))["hypervisor_parity"]
  name              = "domain-controller-${count.index}.${yamldecode(file("seed-variables.yml"))["domain_name"]}"
  target_node       = element(yamldecode(file("hostnames.yml"))["hostnames"],count.index)
  clone             = var.pm_almatemplate_name
  os_type           = "cloud-init"
  bootdisk          = "virtio0"
  cores             = 2
  sockets           = "1"
  cpu               = "host"
  memory            = 4098
  agent             = 1
  disk {
    iothread        = 1
    cache           = "writeback"
    size            = "50G"
    storage         = var.pm_target_storage
    type            = "virtio"
  }
  network {
    model           = "virtio"
    bridge          = "vmbr0"
  }
  lifecycle {
    ignore_changes  = [
      network,
    ]
  }
  # Cloud Init Settings
  ipconfig0 = "ip=${yamldecode(file("hostnames.yml"))["ip_addr"]}.10.1${count.index}/16,gw=${yamldecode(file("hostnames.yml"))["ip_addr"]}.0.1"
  searchdomain = yamldecode(file("seed-variables.yml"))["domain_name"]
  nameserver = "1.1.1.1"
  ciuser = var.pm_cloud_user
  sshkeys = <<EOF
${file("/root/.ssh/id_rsa.pub")}
EOF
}
