resource "proxmox_vm_qemu" "storage_controllers" {
  vmid              = 200+count.index
  count             = yamldecode(file("seed-variables.yml"))["hypervisor_parity"]
  name              = "storage-controller-${count.index}.${yamldecode(file("seed-variables.yml"))["domain_name"]}"
  target_node       = element(yamldecode(file("hostnames.yml"))["hostnames"],count.index)
  clone             = var.pm_almatemplate_name
  os_type           = "cloud-init"
  bootdisk          = "virtio0"
  cores             = 4
  sockets           = "1"
  cpu               = "host"
  memory            = 8192
  agent             = 1
  disk {
    iothread        = 1
    cache           = "writeback"
    size            = "50G"
    storage         = var.pm_target_storage
    type            = "virtio"
  }
  disk {
    iothread        = 1
    cache           = "writeback"
    size            = "500G"
    storage         = var.pm_target_storage
    type            = "virtio"
  }
  network {
    model           = "virtio"
    bridge          = "vmbr0"
  }
  lifecycle {
    ignore_changes  = [
      network,
    ]
  }
  # Cloud Init Settings
  ipconfig0 = "ip=${yamldecode(file("hostnames.yml"))["ip_addr"]}.10.2${count.index}/16,gw=${yamldecode(file("hostnames.yml"))["ip_addr"]}.0.1"
  searchdomain = yamldecode(file("seed-variables.yml"))["domain_name"]
  nameserver = "1.1.1.1"
  ciuser = var.pm_cloud_user
  sshkeys = <<EOF
${file("/root/.ssh/id_rsa.pub")}
EOF

}
