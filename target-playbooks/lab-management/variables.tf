variable "pm_hypervisor_url" {
  type = string
  default = "https://localhost:8006/api2/json"
}
variable "pm_target_storage" {
  type = string
  default = "havamal.lab"
}
variable "pm_user" {
  type = string
  default = "terraform@pve"
}
variable "pm_pass" {
  type = string
  default = "this_account_will_be_destroyed_later"
}
variable "pm_almatemplate_name" {
  type = string
  default = "alma8template.havamal.lab"
}
variable "pm_fedoratemplate_name" {
  type = string
  default = "fedoracoretemplate.havamal.lab"
}
variable "pm_cloud_user" {
  type = string
  default = "administrator"
}
variable "pm_bootdisk" {
  type = string
  default = "virtio0"
}