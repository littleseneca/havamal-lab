#!/bin/bash
root_partition=$(pvdisplay | grep -B 4 pve | head -2 | tail -1 | awk '{print $3}')
if [[ $root_partition == *"nvme"* ]]; 
then
	echo $root_partition | sed -e 's/p[0-9]//' | sed -e 's/dev//' | sed -e 's/\///g'
else
	echo $root_partition | sed -e 's/[0-9]//' | sed -e 's/dev//' | sed -e 's/\///g'
fi