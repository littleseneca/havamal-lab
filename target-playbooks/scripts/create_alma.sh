#!/bin/bash
vm_path="/var/lib/vz/template/qemu/alma8template.qcow2"
new_template=$(( $RANDOM % 50000 + 60000 ))
qm create ${new_template} --memory 2048 --net0 virtio,bridge=vmbr0
qm importdisk ${new_template} ${vm_path} havamal.lab
qm set ${new_template} --scsihw virtio-scsi-single --virtio0 havamal.lab:vm-${new_template}-disk-0
qm set ${new_template} --boot c --bootdisk virtio0
qm set ${new_template} --ide2 havamal.lab:cloudinit
qm set ${new_template} --agent enabled=1
qm set ${new_template} --name="alma8template.havamal.lab"
qm template ${new_template}
# /Script #